#!/usr/bin/env bash

createDir() {
  sudo mkdir $PREFIX
}


createRunScript() {
  if [ -f $PREFIX/run.sh ] ; then
    sudo rm $PREFIX/run.sh
  fi
  sudo touch $PREFIX/run.sh
  echo "#!/usr/bin/env bash" | sudo tee -a $PREFIX/run.sh > /dev/null && \
  echo "" | sudo tee -a $PREFIX/run.sh > /dev/null && \
  echo "cd $PREFIX" | sudo tee -a $PREFIX/run.sh > /dev/null && \
  echo "bin/syscheck -d -cc -lg -m admin@bioinfo.wsu.edu -s admin@bioinfo.wsu.edu -p mail.bioinfo.wsu.edu > /dev/null" | sudo tee -a $PREFIX/run.sh > /dev/null
  sudo chmod u+x $PREFIX/run.sh
}

installSysCheck(){
  sudo cp -R ./* $PREFIX && \
  sudo rm /etc/systemd/system/syscheck.service > /dev/null 2>&1
  sudo touch /etc/systemd/system/syscheck.service
  echo "[Unit]" | sudo tee -a /etc/systemd/system/syscheck.service > /dev/null && \
  echo "Description=SysCheck Service" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "After=network.target" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "[Service]" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "ExecStart=$PREFIX/run.sh" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "[Install]" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null && \
  echo "WantedBy=multi-user.target" | sudo tee -a  /etc/systemd/system/syscheck.service > /dev/null
  createRunScript
  sudo systemctl enable syscheck.service && \
  sudo systemctl restart syscheck.service && \
  echo "Done. You can check the status by running 'sudo systemctl status syscheck.service'"
  echo "A run script '$PREFIX/run.sh' has been created. "
  echo "If you make changes to the run script, make it effective by 'sudo systemctl restart syscheck.service'"
}


read -p "You need to have root access (sudo) to install SysCheck as a systemd service. Continue? [yes/no] " ANS

PREFIX="/opt/syscheck"

if [[ $ANS == "yes" ]] ; then
  
  read -p "Install syscheck in : [$PREFIX] " PREFIX2
  
  if [[ ! -z $PREFIX2 && $PREFIX != $PREFIX2 ]] ; then
    PREFIX=$PREFIX2
  fi
  
  read -p "Installing syscheck to $PREFIX . Continue? [yes/no] " CONTINUE
    if [[ $CONTINUE != "yes" ]] ; then
      echo 'Abort.'
      exit
    fi
  if [ -d $PREFIX ] ; then

    read -p "Directory $PREFIX exists. Overwrite? [yes/no] " ANS2
    if [[ $ANS2 == "yes" ]] ; then
      installSysCheck
    else
      echo 'Abort.'
    fi
  else
    createDir
    installSysCheck
  fi
else
  echo "Exit."
fi
