# SysCheck for Linux

SysCheck is a light weight tool for monitoring Linux systems and is installed as a systemd service that starts when the system boots.


## Download
  git clone https://gitlab.com/chunhuaicheng/syscheck.git

## Install
To install SysCheck as a systemd service, you need to be able to sudo to have root permission. Simply execute the install.sh script will 
walk you through the installation process.

## Customize check options
The install.sh script will create a run.sh which allows you to customize the options about what to check. Simply edit the run.sh and 
restart systemd by 'sudo systemctl restart syscheck.service' to make it effective. The following lists the options you can use in the run.sh
script.


## Options
  -d | --daemon                                        Run as a daemon that constantly perfoms
                                                       the checks. This allows you to send the
                                                       process to the background.

  -i | --interval <int:second>                         When running as a daemon with -d, checks
                                                       are performed every 60 seconds. Specify
                                                       a value (in second) to change the check
                                                       interval.

  -t | --time <int:minute>                             Response time. When running as a daemon
                                                       using -d, the program can be set to respond
                                                       (with emails and/or commands) to an issue
                                                       repeatedly. Specify a non-zero value (in
                                                       minute) to respond repeatedly every specified
                                                       minutes. By default, the response time
                                                       is set to 0 so it only responds once immediately
                                                       (altough you can still get emails repeatedly
                                                       if an issue recovers then occurs again.)

  -cc | --check_common                                 Check common local issues on the host
                                                       using default threshold values. Including
                                                       (1) 1-min load average > 2 * num of CPU
                                                       (2) disk uage > 85% (3) memory usage >
                                                       95% (4) swap usage > 50%. This can be
                                                       combined with other options to change
                                                       the threshold values.

  -m | --email <string>                                Send notification to this email if any
                                                       issue is found. (Requires local server
                                                       having the ability to sent emails.)

  -s | --sender <string>                               Sender of the notification emails.

  -p | --smtp_host <string>                            SMTP host to send the notification emails.
                                                       This option is ignored if Gamil is used
                                                       to send notification.

  -g | --gmail_pass_file <string>                      Use Gmail to send the notification emails.
                                                       You need to apply for an 'App Password'
                                                       at https://myaccount.google.com/apppasswords
                                                       and store the username and password in
                                                       this file. You should change the file
                                                       permission to make sure no one else can
                                                       read this file. The file should only have
                                                       two lines with first line being the gmail
                                                       account whereas the second line being
                                                       the app pass code.

  -r | --run <string>                                  Run this command if any issue is found.

  -o | --output <string>                               Output to the specified file if any issue
                                                       is found.

  -rh | --outputRemoteHost <string>                    Output to the specified file (with -o)
                                                       on a remote host if any issue is found.
                                                       The remote host should have passwordless
                                                       ssh setup to allow writing to the file.

  -cl | --check_load <number:float>                    Check 1-min load average and specify a
                                                       threshold (usually the number of CPU)
                                                       to be flagged as an issue.

  -cd | --check_disk <number:int>                      Check disk usage% and specify a threshold
                                                       percentage (such as 80) to be flagged
                                                       as an issue.

  -cdf | --check_df <number:int>                       Use the command 'df' to check disk usage%
                                                       and specify a threshold percentage (such
                                                       as 80) to be flagged as an issue. (df
                                                       must be available on the system)

  -cm | --check_mem <number:int>                       Check memory usage% and specify a threshold
                                                       percentage (such as 90) to be flagged
                                                       as an issue.

  -cs | --check_swap <number:int>                      Check Swap usage% and specify a threshold
                                                       percentage (such as 90) to be flagged
                                                       as an issue.

  -cfa | --check_file_accessed <file>                  Check if the specified file is accessed
                                                       (only if running in daemon mode). Separate
                                                       files with a comma (,) if checking multiple
                                                       files is desired.

  -cfm | --check_file_modified <file>                  Check if the specified file is modified
                                                       (only if running in daemon mode). Separate
                                                       files with a comma (,) if checking multiple
                                                       files is desired.

  -cfu | --check_file_update <string:'file:seconds'>   Check if the specified file has NOT been
                                                       updated for a certain period of time (Default=24h
                                                       or 86400 seconds if not specified). Separate
                                                       'file:seconds' values with a comma (,)
                                                       if checking multiple files is desired.

  -lg | --check_login                                  Check if there is a new user login (only
                                                       if running in daemon mode).

  -cw | --check_web <string:url>                       Check connection of specified web site(s).
                                                       Please provide a valid URL such as www.google.com.
                                                       Separate sites with a comma (,) if checking
                                                       multiple sites is desired. This check
                                                       can often detect DNS, web server, or database
                                                       server issues with different warning messages.

  -ch | --check_host <string:'host:port'>              Check connection of specified host and
                                                       port. Please provide a valid string such
                                                       as localhost:22 or 127.0.0.1:80. Separate
                                                       hosts and ports with a comma (,) if checking
                                                       multiple hosts is desired.
  -n | --check_nagios <file:script>              Check Nagios plugins. Put all Nagios commands 
                                                       in this file and they will be checked line by line.
  -h | --help                                          Display this help.